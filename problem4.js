const programmingLanguages = [
  "C",
  "C++",
  "Java",
  "Python",
  "Javascript",
  "Golang",
  "Ruby",
  "Perl",
  "Go",
];

function customFind(elements, element) {
  for (let index = 0; index < elements.length; index++) {
    if (elements[index] == element) {
      return true;
    }
  }
  return false;
}

// this function groups users based on their Programming Language mentioned in their designation
function getProgrammingLanguageGroups(users) {
  if (users) {
    const programmingLanguageGroups = {};
    const userNames = Object.keys(users);
    for (let index = 0; index < userNames.length; index++) {
      const userName = userNames[index];
      const designation = users[userName].desgination;
      const keywords = designation.split(" ");
      for (let pointer = 0; pointer < keywords.length; pointer++) {
        const keyword = keywords[pointer];
        if (customFind(programmingLanguages, keyword)) {
          if (!programmingLanguageGroups[keyword]) {
            programmingLanguageGroups[keyword] = [];
          }
          programmingLanguageGroups[keyword].push(userName);
        }
      }
    }
    return programmingLanguageGroups;
  } else {
    return null;
  }
}

module.exports = getProgrammingLanguageGroups;
