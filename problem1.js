// this function finds all users who are interested in playing video games

function getVideoGameUsers(users) {
  if (users) {
    const videoGameUsers = [];
    const userNames = Object.keys(users);
    for (let index = 0; index < userNames.length; index++) {
      const userName = userNames[index];
      let interests = users[userName].interests
        ? users[userName].interests?.[0].split(", ")
        : users[userName].interest?.[0].split(", ");

      for (let pointer = 0; pointer < interests?.length; pointer++) {
        const interest = interests[pointer];
        if (interest.includes("Video Games")) {
          videoGameUsers.push(userName);
        }
      }
    }
    return videoGameUsers;
  } else {
    return [];
  }
}

module.exports = getVideoGameUsers;
