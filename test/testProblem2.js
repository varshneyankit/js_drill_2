const users = require("../data");
const getUsersFromGermany = require("../problem2");

const usersFromGermany = getUsersFromGermany(users);

if (usersFromGermany.length != 0) {
  console.log(usersFromGermany);
} else {
  console.log("Data is empty");
}
