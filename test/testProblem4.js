const users = require("../data");
const getProgrammingLanguageGroups = require("../problem4");

const programmingLanguageGroups = getProgrammingLanguageGroups(users);

if (programmingLanguageGroups != null) {
  console.log(programmingLanguageGroups);
} else {
  console.log("Data is empty");
}
