const users = require("../data");
const getUsersWithMastersDegree = require("../problem3");

const usersWithMastersDegree = getUsersWithMastersDegree(users);

if (usersWithMastersDegree.length != 0) {
  console.log(usersWithMastersDegree);
} else {
  console.log("Data is empty");
}
