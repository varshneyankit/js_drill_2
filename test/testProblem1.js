const users = require("../data");
const getVideoGameUsers = require("../problem1");

const videoGameUsers = getVideoGameUsers(users);

if (videoGameUsers.length != 0) {
  console.log(videoGameUsers);
} else {
  console.log("Data is empty");
}
