// this function finds all users staying in Germany

function getUsersFromGermany(users) {
  if (users) {
    const usersFromGermany = [];
    const userNames = Object.keys(users);
    for (let index = 0; index < userNames.length; index++) {
      const userName = userNames[index];
      const nationality = users[userName].nationality;
      if (nationality == "Germany") {
        usersFromGermany.push(userName);
      }
    }
    return usersFromGermany;
  } else {
    return [];
  }
}

module.exports = getUsersFromGermany;
