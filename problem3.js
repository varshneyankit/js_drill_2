// this function finds all users with Masters Degree

function getUsersWithMastersDegree(users) {
  if (users) {
    const usersWithMastersDegree = [];
    const userNames = Object.keys(users);
    for (let index = 0; index < userNames.length; index++) {
      const userName = userNames[index];
      const qualification = users[userName].qualification;
      if (qualification.includes("Masters")) {
        usersWithMastersDegree.push(userName);
      }
    }
    return usersWithMastersDegree;
  } else {
    return [];
  }
}

module.exports = getUsersWithMastersDegree;
